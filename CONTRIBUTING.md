# Contributing guidelines

## Steps / TL;DR
1. Create an issue of a thing with descriptive title and some description
2. Create a merge request and branch off this issue
3. Do some code
4. Push completed code to the MR and remove `WIP` from the title
5. Assign a maintainer to do code review

## Issues

- Use descriptive titles, e.g. `Add camera rotation`
- Add proper descriptions, going a bit more into detail what needs to be done and why
- OPTIONAL: Add checkboxes of each step and mark them complete when done in code.

## Branch naming

- Try to keep your branch names fairly short and use a prefix and the issue number.
- Make the names descriptive enough.
  - Using GitLab's functionality `Create a merge request and branch`
in the issue can take care of this easily––as long as the issue title is properly made


There are most likely three types of branches that are mainly used:
1. Feature – `feature/1-add-camera-rotation`
2. Bug – `bugfix/2-stuttering-animation` 
3. Improvement – `improvement/3-remove-unnecessary-code` 

## Commits

- Mention issue number in each commit message, e.g. `#1 – Add functionality to be able to zoom the camera`
- State **why** instead of **what** in the commit message
  -  `Remove code` vs `Remove code that was breaking the wiener spinning`

## Merge Requests

- Try to open a MR immediately when starting to work on an issue
- Use `WIP` in the title when still working on something
- Remove `WIP` from the title when completed work and pushed all the code
- Assign a maintainer to do a code review



