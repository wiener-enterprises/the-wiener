# [Behold the Wiener](https://wiener.enterprises/)

The beginning of Wiener Enterprises.

## The Wiener is a digital playground

This is supposed to be a WebGL playground with new features added
every once in a while.

Get a local env setup:
```
  npm i
  npm start
  Go to http://localhost:1234
```

## Next up in pipeline (in no specific order)

- [ ] Better, smoother wiener, i.e. new 3D model
- [ ] Wrap the wiener (do we want a bun???)
- [ ] Dripping wiener, i.e. gravity to toppings, ketchup droplets, etc.
  - Should also react to the spinning
- [ ] More toppings
  - Maybe the user can add them by themself?
- [ ] GitLab CI/CD to automatically deploy new code to the server

--------------
- [x] ~~Get a closer inspection on the wiener, i.e. camera zoom and rotation control~~ v1.1.0
- [x] ~~Spin the wiener, i.e. mouse control~~ v1.1.0