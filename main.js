import * as THREE from 'three'
import GLTFLoader from 'three-gltf-loader'
import wiener from './wiener-2.gltf'
import OrbitControls from 'orbit-controls-es6'

const scene = new THREE.Scene()
const camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 1000)
const loader = new GLTFLoader()
const raycaster = new THREE.Raycaster()
const mouse = new THREE.Vector2()

const renderer = new THREE.WebGLRenderer()
const canvas = renderer.domElement
const controls = new OrbitControls(camera, canvas)
controls.enableZoom = true

renderer.setSize(window.innerWidth, window.innerHeight)
renderer.setClearColor(0x30f2a1, 1)
document.body.appendChild(renderer.domElement)

camera.position.z = 5
controls.maxDistance = 12
controls.minDistance = 1

controls.update()

let object = null

loader.load(wiener, function(gltf) {
  object = gltf.scene

  object.scale.x = object.scale.y = object.scale.z = 0.75
  scene.add(object)
})

document.addEventListener('touchstart', event => {
  event.preventDefault()
}, { passive: false })

document.addEventListener('touchend', event => {
  event.preventDefault()
}, { passive: false })

document.addEventListener('touchmove', event => {
  event.preventDefault()
}, { passive: false })

let isDragging = false
canvas.addEventListener('mousedown', () => {
  controls.enabled = false
  isDragging = true
}, false)

canvas.addEventListener('mouseup', () => {
  controls.enabled = true
  isDragging = false
}, false)

canvas.addEventListener('mousemove', event => {
  onMouseMove(event)

  if (isDragging) {
    object.rotation.y += event.movementX * 0.005;
    object.rotation.x += event.movementY * 0.005;
  }
}, false)

let previousTouch = null

canvas.addEventListener('touchstart', event => {
  const touch = event.touches[0]

  controls.enabled = false
  isDragging = true
  previousTouch = touch
}, false)

canvas.addEventListener('touchend', event => {
  const touch = event.touches[0]

  controls.enabled = true
  isDragging = false
  previousTouch = touch
}, false)

canvas.addEventListener('touchmove', event => {
  const touch = event.touches[0]

  if (isDragging) {
    const movementX = previousTouch ? touch.screenX - previousTouch.screenX : 0
    const movementY = previousTouch ? touch.screenY - previousTouch.screenY : 0

    object.rotation.y += movementX * 0.005;
    object.rotation.x += movementY * 0.005;
  }

  previousTouch = touch
}, false)

function onMouseMove(event) {
	// calculate mouse position in normalized device coordinates
	// (-1 to +1) for both components
	mouse.x = ( event.clientX / window.innerWidth ) * 2 - 1;
	mouse.y = - ( event.clientY / window.innerHeight ) * 2 + 1;
}

function animate() {
  requestAnimationFrame(animate)

  checkHoverOnObject()

  if (!isDragging) {
    if (object) {
      object.rotation.y += 0.0025
      object.rotation.x += 0.0025
    }
  }

  renderer.render(scene, camera)
}

function checkHoverOnObject() {
  raycaster.setFromCamera(mouse, camera);

  const intersects = raycaster.intersectObjects(scene.children, true)
  const bodyElement = document.getElementsByTagName('body')[0]
  if (intersects.length) {
    bodyElement.style.cursor = 'pointer'

  } else if (bodyElement.style.cursor !== 'default') {
    bodyElement.style.cursor = 'default'
  }
}

animate()

const spotLight = new THREE.SpotLight(0xcccccc, 7, 120)
spotLight.position.set(50, 10, 50);
scene.add(spotLight)

const hemishpere = new THREE.HemisphereLight(0xffffff, 0x000000, 0.5);
scene.add(hemishpere)
